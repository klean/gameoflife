﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

/*
 *  * 
 * GAME OF LIFE
 * 
 * 0,1 neighbors = die
 * 2,3 neigh = life
 * 4 or more = death
 * 
 * each pixel = 2*2
 * 
 * size = 800 * 600
 * dead = black
 * alive = white
 * 
 * reads in the initial popn from text file
 * */


namespace GOL
{
    public partial class Form1 : Form
    {


        Graphics g1;
        Brush b1;
        int size_default;
        Color color_default;

        StreamReader infile;
        OpenFileDialog openFileDialog1;
        bool haschanged;
        bool timeron = false;

        int[] old_life = new int[120000];
        int[] new_life = new int[120000];

        int pixsize = 2;


        public Form1()
        {
            InitializeComponent();

            //create default data
            pictureBox1.Width = 800;
            pictureBox1.Height = 600;
            g1 = pictureBox1.CreateGraphics(); //initialize the graphics
            color_default = Color.White;
            size_default = 2;
            haschanged = true;

        }
        private void getdata()
        {
            // read the file

            g1.Clear(Color.Black);
            infile = File.OpenText(openFileDialog1.FileName);

            string s = infile.ReadLine();
           

            b1 = new SolidBrush(color_default);


            while (s != null)
            {
                string[] t = s.Split(' ');
                int x = Convert.ToInt16(t[0]);
                int y = Convert.ToInt16(t[1]);

                b1 = new SolidBrush(Color.White);
                if (t.Length > 2)
                {
                    int r = Convert.ToInt16(t[2]);
                    int g = Convert.ToInt16(t[3]);
                    int b = Convert.ToInt16(t[4]);

                    b1 = new SolidBrush(Color.FromArgb(r, g, b));
                }

            //    MessageBox.Show("Drawn");
                drawOne(x, y);
                //make the zombie alive

                int xaxis = x;
                int yaxix = y * 400;
                drawOne(x, y);
            
                int pos = xaxis+yaxix;
            
                old_life[pos] = 1;

                int xinv = pos/400;
              
                int yinv = pos - (400*xinv);
             


                s = infile.ReadLine();
                
            }
            
            infile.Close();
        }

        //this draws 
        private void drawOne(int x_in, int y_in)
        {
            
            // * 5 just to make the image spread out
            int x = (x_in * pixsize);
            int y = (y_in * pixsize);
           
            g1.FillRectangle(b1, x, y, pixsize, pixsize);
           // MessageBox.Show(" x y " + x + " " + y);


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //make all zombies dead at first

            alldead();
        }
        private void alldead()
        {
            for (int x = 0; x < 120000; x++)
            {
                old_life[x] = 0;
                new_life[x] = 0;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            
            //open the file
            openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Text Files | *.txt";
            openFileDialog1.ShowDialog();
            g1.Clear(Color.Black);
            alldead();
            getdata();
            
            
            

        }



        private void button2_Click(object sender, EventArgs e)
        {
            



            
            checkneighbors();


               

                //see if it has changed
            checklifechange();

                //copy the new data
            updatenewlife();
                

        }

        private void checkneighbors()
        {
            //continue until all is dead
            haschanged = false;


            // int x = Convert.ToInt16(textBox1.Text);
            for (int x = 0; x < 120000; x++)
            {
               
                int neigh = 0;


              
                int xinv = x % 400;
                int yinv = (x / 400);
               

                //check for neighbors
                if ((x / 400) != 0 && (x / 399) != 0 && x > 400 && x < (120000 - 401))
                {
                    neigh = old_life[x + 1] + old_life[x - 1] + old_life[x - 400] + old_life[x + 400] + old_life[x - 399] + old_life[x + 401] + old_life[x - 401]+old_life[x + 399];
                   



                }

                if (old_life[x] == 1)
                {

                    if (neigh < 2)
                    {
                      
                        b1 = new SolidBrush(Color.Black);

                        drawOne(xinv, yinv);
                        new_life[x] = 0;
                    }
                    else if (neigh < 4)
                    {


                        new_life[x] = 1;
                        b1 = new SolidBrush(Color.White);
                        drawOne(xinv, yinv);

                    }
                    else
                    {
                      
                        b1 = new SolidBrush(Color.Black);
                        drawOne(xinv, yinv);
                        new_life[x] = 0;
                    }
                   
                }else
                if (neigh > 2 && neigh < 4)
                {
                   
                    new_life[x] = 1;
                    b1 = new SolidBrush(Color.White);
                    drawOne(xinv, yinv);

                }

            }
        }

        private void checklifechange()
        {
            for (int x = 0; x < 120000; x++)
            {
                if (old_life[x] != new_life[x])
                {
                    haschanged = true;
                    break;
                }
            }
        }

        private void updatenewlife()
        {
            for (int x = 0; x < 120000; x++)
            {
                old_life[x] = new_life[x];
            }

        }
        private void button4_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           
            timeron = !timeron;
          

            checkneighbors();
             
            checklifechange();
                //see if it has changed
                

                //copy the new data
            updatenewlife();
              
        }

        private void button5_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

       

        private void button3_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            timer1.Enabled = false;
            int x = e.X;
            int y = e.Y;
            

            int xaxis = x/pixsize;
            int yaxix = y/pixsize * 400;
            int pos = xaxis + yaxix;
         
            if (old_life[pos] == 0)
            {
                new_life[pos] = 1;
                old_life[pos] = 1;
                b1 = new SolidBrush(Color.White);
                drawOne(x / pixsize, y / pixsize);

            }
            else
            {
                new_life[pos] = 0;
                old_life[pos] = 0;
                b1 = new SolidBrush(Color.Black);
                drawOne(x / pixsize, y / pixsize);
            }



        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            pixsize = trackBar1.Value;
            updatedisplay();
            label1.Text = "Size " + pixsize;
        }

        private void updatedisplay()
        {
            g1.Clear(Color.Black);
            checkneighbors();

            checklifechange();
            //see if it has changed


            //copy the new data
            updatenewlife();

        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            trackBar2.Maximum = 10;
            trackBar2.Minimum = 1;

            int  speed = 100 + 100*(10 - trackBar2.Value);
            label2.Text = "Speed = " + speed + " millisecond";

            timer1.Interval = speed;
        }
        
    }
}
